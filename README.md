Launch `./fetch_data.sh`

Wait for it...

Or if you hate waiting and already have got files remove `./data` folder and make symlink for it

`rm -r ./data && ln -s ${YOUR_FOLDER_WITH_DATA} ./data`

Launch `python3 top10actors.py`

Expected output is
```
$ python top10actors.py 
1. Name1 (rating1)
2. Name2 (rating2)
...

```