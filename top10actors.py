import gzip
import csv
import os

dataPath = './data'
outputPath = './output'

nameFile = dataPath + '/name.basics.tsv.gz'
akasFile = dataPath + '/title.akas.tsv.gz'
principalsFile = dataPath + '/title.principals.tsv.gz'
ratingsFile = dataPath + '/title.ratings.tsv.gz'
actorsWithRatingsFile = outputPath + '/actorsFiltered.tsv.gz'


def header_map(f):
    hlist = f.readline().rstrip().split('\t')
    return {i: hlist.index(i) for i in hlist}


def join(file1, file2, join_field, output_fields1, output_fields2, write_to_output_cb, output_file_path):
    def check_requested_fields(hm1, hm2):
        assert (join_field in hm1 and join_field in hm2), "Join field must be present in both datasets"
        for field in output_fields1:
            assert (field in hm1), "All fields in output_fields1 must be in the dataset of file1. {} is not available".format(field)
        for field in output_fields2:
            assert (field in hm2), "All fields in output_fields2 must be in the dataset of file2. {} is not available".format(field)

    def get_output_file():
        csv_file = gzip.open(output_file_path, mode='wt')
        fieldnames = [join_field] + output_fields1[:] + output_fields2[:]
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames, delimiter="\t", quoting=csv.QUOTE_NONE)
        writer.writeheader()

        return writer

    f1 = gzip.open(file1, mode='rt')
    f2 = gzip.open(file2, mode='rt')

    hmf1 = header_map(f1)
    hmf2 = header_map(f2)

    check_requested_fields(hmf1, hmf2)

    writer = get_output_file()

    write_to_output_cb(f1, f2, writer, hmf1, hmf2, join_field)


def write_output_actors_with_rating(r1, r2, writer, r1header, r2header, join_field):
    for main_line in csv.reader(r2, delimiter='\t', quotechar="'"):
        for line in csv.reader(r1, delimiter='\t', quotechar="'"):
            row = {
                join_field: line[r1header['tconst']],
                'nconst': line[r1header['nconst']],
                'averageRating': main_line[r2header['averageRating']]
            }
            if main_line[r2header[join_field]] == line[r1header[join_field]] \
                    and line[r1header['category']] == 'actor':  # and int(main_line[r2header['numVotes']]) > 25000:
                writer.writerow(row)
            elif int(main_line[r2header[join_field]][2:]) > int(line[r1header[join_field]][2:]):
                continue
            else:
                break


def get_top_actors(amount):
    actors_dict = {}
    f = gzip.open(actorsWithRatingsFile, mode='rt')
    f.readline()

    for line in csv.reader(f, delimiter='\t'):
        if line[1] in actors_dict:
            actors_dict[line[1]] = actors_dict[line[1]] + [float(line[2])]
        else:
            actors_dict[line[1]] = [float(line[2])]

    for actorId in actors_dict.keys():
        actors_dict[actorId] = sum(actors_dict[actorId]) / len(actors_dict[actorId])

    sorted_list = sorted(actors_dict.items(), key=lambda x: x[1], reverse=True)

    return dict(sorted_list[0:amount])


def show_top10_actors():
    top10 = get_top_actors(10)

    f = gzip.open(nameFile, mode='rt')
    hm = header_map(f)

    for line in csv.reader(f, delimiter='\t', quotechar="'"):
        if line[hm['nconst']] in top10.keys():
            top10[line[hm['nconst']]] = line[hm['primaryName']], top10[line[hm['nconst']]]

    i = 0
    for name, rating in top10.values():
        i += 1
        print("{}. {} ({})".format(i, name, rating))


if not os.path.isfile(actorsWithRatingsFile):
    join(principalsFile, ratingsFile, 'tconst', ['nconst'], ['averageRating'], write_output_actors_with_rating,
         actorsWithRatingsFile)

show_top10_actors()
