#! /bin/bash

mkdir -p ./data
cd ./data
wget datasets.imdbws.com/{title.akas.tsv.gz,title.principals.tsv.gz,title.ratings.tsv.gz,name.basics.tsv.gz}
